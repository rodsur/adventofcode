package ${PACKAGE_NAME};

import net.rodsur.aoc.common.aocPuzzle;

import java.util.List;

public class ${NAME} implements aocPuzzle {
    @Override
    public String solvePart1(List<String> input) {
        TrebuchetCalibration trebuchetCalibration = new TrebuchetCalibration(input);
        return String.valueOf(trebuchetCalibration.findAndAddCalibrationSums(false));
    }

    @Override
    public String solvePart2(List<String> input) {
        TrebuchetCalibration trebuchetCalibration = new TrebuchetCalibration(input);
        return String.valueOf(trebuchetCalibration.findAndAddCalibrationSums(true));
    }
}
