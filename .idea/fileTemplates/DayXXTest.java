package ${PACKAGE_NAME};

import net.rodsur.aoc.common.inputLoader;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;


class ${NAME} {

    static DayXX puzzle;
    private List<String> input;
    private String output;

    @BeforeAll
    public static void setup() {
        puzzle = new DayXX();
    }

    @AfterEach
    void clearOutput() {
        output = "0";
    }

    @Test
    void solvePart1Example() {
        input = inputLoader.loadFile("src/test/resources/2023/DayXXPart1Example");
        output = puzzle.solvePart1(input);

        assertEquals("", output);
    }

    @Test
    void solvePart1() {
        input = inputLoader.loadFile("src/test/resources/2023/DayXX");
        output = puzzle.solvePart1(input);

        assertEquals("", output);
    }
    @Test
    void solvePart2Example() {
        input = inputLoader.loadFile("src/test/resources/2023/DayXXPart2Example");
        output = puzzle.solvePart2(input);

        assertEquals("", output);
    }

    @Test
    void solvePart2() {
        input = inputLoader.loadFile("src/test/resources/2023/DayXX");
        output = puzzle.solvePart2(input);

        assertEquals("", output);
    }
}