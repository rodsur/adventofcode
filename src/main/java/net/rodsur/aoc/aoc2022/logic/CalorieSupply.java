package net.rodsur.aoc.aoc2022.logic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CalorieSupply {

    private final List<Integer> caloriesCarried;

    public CalorieSupply(List<String> inputList) {
        caloriesCarried = generateSortedListOfCalories(inputList);
    }

    private List<Integer> generateSortedListOfCalories(List<String> input) {
        List<Integer> calorieList = new ArrayList<>();
        int calorieAmount = 0;

        for (String line : input) {
            if (!line.isBlank()) {
                int calories = Integer.parseInt(line);
                calorieAmount += calories;
            } else {
                calorieList.add(calorieAmount);
                calorieAmount = 0;
            }
        }

        calorieList.sort(Collections.reverseOrder());
        return calorieList;
    }

    public int getNthMostCarried(int n) {
        int nthMostCarried = 0;

        for (int i = 0; i < n && i < caloriesCarried.size(); i++) {
            nthMostCarried += caloriesCarried.get(i);
        }

        return nthMostCarried;
    }
}
