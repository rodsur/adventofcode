package net.rodsur.aoc.aoc2022.logic;

import java.util.*;


public class CampCleanup {

    final private List<String> assignmentPairs;

    public CampCleanup(List<String> input){
        assignmentPairs = input;
    }

    public int getNumberOfCompletelyOverlappingAssignments() {
        int overlappingAssignments = 0;
        for (String pair : assignmentPairs){
            int[] intPair = convertPairStringToInt(pair);
            Set<Integer> assignmentRange1 = createSetFromInts(intPair[0],intPair[1]);
            Set<Integer> assignmentRange2 = createSetFromInts(intPair[2],intPair[3]);
            if (assignmentRange1.containsAll(assignmentRange2) || assignmentRange2.containsAll(assignmentRange1)) {
                overlappingAssignments++;
            }
        }
        return overlappingAssignments;
    }

    private Set<Integer> createSetFromInts(int min, int max) {
        Set<Integer> outputSet = new HashSet<>();
        for (int i = min; i <= max; i++) {
            outputSet.add(i);
        }
        return outputSet;
    }
    private int[] convertPairStringToInt(String pairs) {
        int[] outputArray = new int[4];
        String[] pairArray = pairs.split("[-,]");
        for (int i = 0; i<outputArray.length; i++){
            outputArray[i] = Integer.parseInt(pairArray[i]);
        }
        return outputArray;
    }

    public int getNumberOfOverlappingAssignments() {
        int overlappingAssignments = 0;
        for (String pair : assignmentPairs){
            int[] intPair = convertPairStringToInt(pair);
            Set<Integer> assignmentRange1 = createSetFromInts(intPair[0],intPair[1]);
            Set<Integer> assignmentRange2 = createSetFromInts(intPair[2],intPair[3]);
            if (!Collections.disjoint(assignmentRange1,assignmentRange2)) {
                overlappingAssignments++;
            }
        }
        return overlappingAssignments;
    }
}
