package net.rodsur.aoc.aoc2022.logic;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Rucksack {

    private final List<String> rucksack;

    private enum alphabet {
        a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z;

        public static int getNum(char target) {
            return valueOf(String.valueOf(target)).ordinal()+1;
        }
    }

    public Rucksack (List<String> input) {
        rucksack = input;
    }

    public int FindPriorityOfMisplacedItems() {
        int priority = 0;

        List<List<Set<Character>>> listOfSets = generateListsOfSetsOfStringsFromInput(rucksack);

        for (List<Set<Character>> list : listOfSets) {
            priority += getPriorityOfCommonItems(list);
        }

        return priority;
    }

    private int getPriorityOfCommonItems(List<Set<Character>> list) {
        int priority = 0;
        Set<Character> controlSet = list.get(0);
        for (Set<Character> characterSet : list) {
            controlSet.retainAll(characterSet);
        }
        if (!controlSet.isEmpty()) {
            for (Character character : controlSet) {
                priority += alphabet.getNum(character);
            }
        }
        return priority;
    }

    public int FindSumOfBadgesForThreeElfGroups() {
        int priority = 0;

        List<List<Set<Character>>> listOfThreeGroupSets = new ArrayList<>();

        for (int i = 0; i < rucksack.size(); i += 3 ) {
            listOfThreeGroupSets.add(generateThreeCharacterSetsFromListAndIndex(rucksack, i));
        }
        for (List<Set<Character>> characterSets : listOfThreeGroupSets) {
            priority += getPriorityOfCommonItems(characterSets);
        }

        return priority;
    }

    private List<Set<Character>> generateThreeCharacterSetsFromListAndIndex(List<String> rucksack, int index) {
        List<Set<Character>> outputList = new ArrayList<>();
        outputList.add(generateCharacterSetFromString(rucksack.get(index)));
        outputList.add(generateCharacterSetFromString(rucksack.get(index + 1)));
        outputList.add(generateCharacterSetFromString(rucksack.get(index + 2)));
        return outputList;
    }


    private List<List<Set<Character>>> generateListsOfSetsOfStringsFromInput(List<String> inputList) {
        List<List<Set<Character>>> outputList = new ArrayList<>();
        for (String line : inputList) {
            List<Set<Character>> listOfSets = new ArrayList<>();

            String left = line.substring(0, line.length()/2);
            String right = line.substring(line.length()/2);

            listOfSets.add(generateCharacterSetFromString(left));
            listOfSets.add(generateCharacterSetFromString(right));

            outputList.add(listOfSets);
        }
        return outputList;
    }

    private Set<Character> generateCharacterSetFromString(String inputString) {
        Character[] charArray = inputString.chars().mapToObj(c -> (char)c).toArray(Character[]::new);
        return new HashSet<>(List.of(charArray));
    }
}
