package net.rodsur.aoc.aoc2022.logic;

import java.util.List;
import java.util.Map;

public class StrategyGuide {

    private static final Map<String, String> OpponentsHand = Map.of("Rock","A","Paper","B","Scissors","C");
    private static final Map<String, String> YourHand = Map.of("Rock","X","Scissors","Z","Paper","Y");
    private static final Map<String, String> MatchOutcome = Map.of("Lose","X","Draw","Y","Win","Z");

    private final List<String> strategyGuide;

    public StrategyGuide(List<String> inputList) {
        strategyGuide = inputList;
    }

    public int getScoreForDayPart1() {
        int score = 0;

        for (String line : strategyGuide) {
            score += getScoreForMatch(line);
        }

        return score;
    }

    public int getScoreForDayPart2() {
        int score = 0;

        for (String line : strategyGuide) {
            score += getScoreForMatchPart2(line);
        }

        return score;
    }

    private int getScoreForMatch(String line) {
        int score = 0;
        String[] strategyChoices = line.split(" ");
        String yourMove = strategyChoices[1];
        String opponentsMove = strategyChoices[0];
        if (yourMove.equals(YourHand.get("Rock"))) {
            score += 1;
            if (opponentsMove.equals(OpponentsHand.get("Rock"))) {
                score += 3;
            } else if (opponentsMove.equals(OpponentsHand.get("Scissors"))) {
                score += 6;
            }
        } else if (yourMove.equals(YourHand.get("Scissors"))) {
            score += 3;
            if (opponentsMove.equals(OpponentsHand.get("Paper"))) {
                score += 6;
            } else if (opponentsMove.equals(OpponentsHand.get("Scissors"))) {
                score += 3;
            }
        } else if (yourMove.equals(YourHand.get("Paper"))) {
            score += 2;
            if (opponentsMove.equals(OpponentsHand.get("Rock"))) {
                score += 6;
            } else if (opponentsMove.equals(OpponentsHand.get("Paper"))) {
                score += 3;
            }
        }
        return score;
    }

    private int getScoreForMatchPart2(String line) {
        int score = 0;
        String[] strategyChoices = line.split(" ");
        String matchResult = strategyChoices[1];
        String opponentsMove = strategyChoices[0];
        if (matchResult.equals(MatchOutcome.get("Lose"))) {
            score += 0;
            if (opponentsMove.equals(OpponentsHand.get("Rock"))) {
                score += 3;
            } else if (opponentsMove.equals(OpponentsHand.get("Scissors"))) {
                score += 2;
            } else if (opponentsMove.equals(OpponentsHand.get("Paper"))) {
                score += 1;
            }
        } else if (matchResult.equals(MatchOutcome.get("Draw"))) {
            score += 3;
            if (opponentsMove.equals(OpponentsHand.get("Rock"))) {
                score += 1;
            } else if (opponentsMove.equals(OpponentsHand.get("Scissors"))) {
                score += 3;
            } else if (opponentsMove.equals(OpponentsHand.get("Paper"))) {
                score += 2;
            }
        } else if (matchResult.equals(MatchOutcome.get("Win"))) {
            score += 6;
            if (opponentsMove.equals(OpponentsHand.get("Rock"))) {
                score += 2;
            } else if (opponentsMove.equals(OpponentsHand.get("Scissors"))) {
                score += 1;
            } else if (opponentsMove.equals(OpponentsHand.get("Paper"))) {
                score += 3;
            }
        }
        return score;
    }
}
