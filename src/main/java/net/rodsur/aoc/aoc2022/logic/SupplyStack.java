package net.rodsur.aoc.aoc2022.logic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Stack;

public class SupplyStack {

    private final List<Stack<Character>> containerStacks;
    private final List<String> instructions;

    public SupplyStack(List<String> input) {
        int linebreak = input.indexOf("");
        containerStacks = GenerateStacksFromInput(input.subList(0,linebreak));
        instructions = input.subList(linebreak, input.size());
    }

    private List<Stack<Character>> GenerateStacksFromInput(List<String> input){
        int OFFSET = 1;
        int SPACE = 4;
        Collections.reverse(input);
        List<Stack<Character>> containerStacks = new ArrayList<>();
        for (int i = OFFSET; i < input.get(0).length(); i +=SPACE) {
            containerStacks.add(new Stack<>());
        }
        input.remove(0);
        for(String line : input) {
            for (int i = OFFSET; i < line.length(); i += SPACE) {
                Stack<Character> container = containerStacks.get((i-OFFSET)/4);
                if (line.charAt(i)!= ' '){
                    container.add(line.charAt(i));
                }

            }
        }
        return containerStacks;
    }

    public void executeInstructions(int craneModel) {
        for (String line : instructions) {
            Stack<Character> tempStack = new Stack<>();
            String[] explodedLine = line.split(" ");

            int sizeToMove = Integer.parseInt(explodedLine[1]);
            int moveFrom = Integer.parseInt(explodedLine[3])-1;
            int moveTo = Integer.parseInt(explodedLine[5])-1;

            Stack<Character> containerToMoveFrom = containerStacks.get(moveFrom);
            Stack<Character> containerToMoveTo = containerStacks.get(moveTo);

            if (craneModel == 9000) {
                moveDirectly(sizeToMove, containerToMoveFrom, containerToMoveTo);
            } else if (craneModel == 9001) {
                moveDirectly(sizeToMove, containerToMoveFrom, tempStack);
                moveDirectly(sizeToMove, tempStack, containerToMoveTo);
            }

        }
    }

    private static void moveDirectly(int sizeToMove, Stack<Character> containerToMoveFrom, Stack<Character> containerToMoveTo) {
        for (int i = sizeToMove; i > 0; i--) {
            if(!containerToMoveFrom.empty()) {
                containerToMoveTo.add(containerToMoveFrom.pop());
            }
        }
    }

    public String getTopContainers() {
        StringBuilder outputString = new StringBuilder();
        for (Stack<Character> container : containerStacks) {
            if (!container.empty()){
                String toAdd = container.peek().toString();
                outputString.append(toAdd);
            }
        }
        return outputString.toString();
    }
}
