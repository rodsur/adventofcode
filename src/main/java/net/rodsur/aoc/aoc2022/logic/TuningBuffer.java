package net.rodsur.aoc.aoc2022.logic;

import java.util.LinkedList;
import java.util.List;

public class TuningBuffer {

    String input;

    public TuningBuffer(List<String> input){
        this.input = input.get(0);
    }

    public int findStartOfPacketMarker(){
        LinkedList<Character> inputBuffer = new LinkedList<>();
        for (int position = 0; position < input.length(); position++) {
            inputBuffer.add(input.charAt(position));
            if (inputBuffer.size() == 4) {
                if (inputBuffer.stream().distinct().count() == 4) {
                    return position+1;
                }
                inputBuffer.removeFirst();
            }
        }
        return -1;
    }

    public int findStartOfMessageMarker(){
        LinkedList<Character> inputBuffer = new LinkedList<>();
        for (int position = 0; position < input.length(); position++) {
            inputBuffer.add(input.charAt(position));
            if (inputBuffer.size() == 14) {
                if (inputBuffer.stream().distinct().count() == 14) {
                    return position+1;
                }
                inputBuffer.removeFirst();
            }
        }
        return -1;
    }
}
