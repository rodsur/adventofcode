package net.rodsur.aoc.aoc2022.puzzles;

import net.rodsur.aoc.aoc2022.logic.CalorieSupply;
import net.rodsur.aoc.common.aocPuzzle;

import java.util.List;

public class Day01 implements aocPuzzle {

    @Override
    public String solvePart1(List<String> input) {
        CalorieSupply calorieSupply = new CalorieSupply(input);
        return String.valueOf(calorieSupply.getNthMostCarried(1));
    }

    @Override
    public String solvePart2(List<String> input) {
        CalorieSupply calorieSupply = new CalorieSupply(input);
        return String.valueOf(calorieSupply.getNthMostCarried(3));
    }
}