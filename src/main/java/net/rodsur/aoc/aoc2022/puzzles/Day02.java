package net.rodsur.aoc.aoc2022.puzzles;

import net.rodsur.aoc.aoc2022.logic.StrategyGuide;
import net.rodsur.aoc.common.aocPuzzle;

import java.util.List;

public class Day02 implements aocPuzzle {
    @Override
    public String solvePart1(List<String> input) {
        StrategyGuide strategyGuide = new StrategyGuide(input);
        return String.valueOf(strategyGuide.getScoreForDayPart1());
    }

    @Override
    public String solvePart2(List<String> input) {
        StrategyGuide strategyGuide = new StrategyGuide(input);
        return String.valueOf(strategyGuide.getScoreForDayPart2());
    }
}
