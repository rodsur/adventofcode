package net.rodsur.aoc.aoc2022.puzzles;

import net.rodsur.aoc.aoc2022.logic.Rucksack;
import net.rodsur.aoc.common.aocPuzzle;

import java.util.*;

public class Day03 implements aocPuzzle {



    @Override
    public String solvePart1(List<String> input) {

        Rucksack rucksack = new Rucksack(input);
        return String.valueOf(rucksack.FindPriorityOfMisplacedItems());


    }

    @Override
    public String solvePart2(List<String> input) {
        Rucksack rucksack = new Rucksack(input);
        return String.valueOf(rucksack.FindSumOfBadgesForThreeElfGroups());
    }


}
