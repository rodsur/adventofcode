package net.rodsur.aoc.aoc2022.puzzles;

import net.rodsur.aoc.aoc2022.logic.CampCleanup;
import net.rodsur.aoc.common.aocPuzzle;

import java.util.List;

public class Day04 implements aocPuzzle {
    @Override
    public String solvePart1(List<String> input) {
        CampCleanup campCleanup = new CampCleanup(input);
        return String.valueOf(campCleanup.getNumberOfCompletelyOverlappingAssignments());
    }

    @Override
    public String solvePart2(List<String> input) {
        CampCleanup campCleanup = new CampCleanup(input);
        return String.valueOf(campCleanup.getNumberOfOverlappingAssignments());
    }
}
