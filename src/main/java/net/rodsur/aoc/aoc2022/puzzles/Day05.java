package net.rodsur.aoc.aoc2022.puzzles;

import net.rodsur.aoc.aoc2022.logic.SupplyStack;
import net.rodsur.aoc.common.aocPuzzle;

import java.util.List;

public class Day05 implements aocPuzzle {
    @Override
    public String solvePart1(List<String> input) {
        SupplyStack supplyStack = new SupplyStack(input);
        supplyStack.executeInstructions(9000);
        return supplyStack.getTopContainers();
    }

    @Override
    public String solvePart2(List<String> input) {
        SupplyStack supplyStack = new SupplyStack(input);
        supplyStack.executeInstructions(9001);
        return supplyStack.getTopContainers();
    }
}
