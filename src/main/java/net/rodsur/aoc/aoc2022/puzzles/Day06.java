package net.rodsur.aoc.aoc2022.puzzles;

import net.rodsur.aoc.aoc2022.logic.TuningBuffer;
import net.rodsur.aoc.common.aocPuzzle;

import java.util.List;

public class Day06 implements aocPuzzle {
    @Override
    public String solvePart1(List<String> input) {
        TuningBuffer tuningBuffer = new TuningBuffer(input);
        return String.valueOf(tuningBuffer.findStartOfPacketMarker());

    }

    @Override
    public String solvePart2(List<String> input) {
        TuningBuffer tuningBuffer = new TuningBuffer(input);
        return String.valueOf(tuningBuffer.findStartOfMessageMarker());
    }
}
