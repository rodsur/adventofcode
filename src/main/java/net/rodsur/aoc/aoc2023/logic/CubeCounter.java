package net.rodsur.aoc.aoc2023.logic;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CubeCounter {
    private final List<String> input;
    private final Map<String, Integer> cubeLimits = Map.of(
            "red",12,
            "green", 13,
            "blue", 14
    );

    public CubeCounter(List<String> input){
        this.input = input;
    }

    public int findSumOfValidGames() {
        int sumOfValidGames = 0;
        for (String gameLine : input) {
            boolean gameIsValid = true;
            gameLine = gameLine.replace("Game ", "");
            String[] splitGameLine = gameLine.split(":");
            int gameValue = Integer.parseInt(splitGameLine[0]);
            for (String round : splitGameLine[1].split(";")) {
                for (String hand : round.split(",")) {
                    hand = hand.trim();
                    int amount = Integer.parseInt(hand.split(" ")[0]);
                    String color = hand.split(" ")[1];
                    if (amount > cubeLimits.get(color)){
                        gameIsValid = false;
                        break;
                    }
                }
                if (!gameIsValid) {
                    break;
                }
            }
            if (gameIsValid) {
                sumOfValidGames += gameValue;
            }
        }
        return sumOfValidGames;
    }

    public int findPowersOfMinimumCubes(){
        int sumOfPowers = 0;
        for (String gameLine : input) {
            int powers = 1;
            String splitGameLine = gameLine.split(":")[1];
            HashMap<String, Integer> minimumCubes = new HashMap<>();
            for(String round : splitGameLine.split(";")) {
                for(String hand : round.split(",")) {
                    hand = hand.trim();
                    int amount = Integer.parseInt(hand.split(" ")[0]);
                    String color = hand.split(" ")[1];
                    if (!minimumCubes.containsKey(color)) {
                        minimumCubes.put(color, amount);
                    } else if (minimumCubes.get(color) < amount) {
                        minimumCubes.replace(color, amount);
                    }
                }
            }
            for (Integer amount : minimumCubes.values()) {
                powers *= amount;
            }
            sumOfPowers += powers;
        }
        return sumOfPowers;
    }
}
