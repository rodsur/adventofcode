package net.rodsur.aoc.aoc2023.logic;

import java.util.ArrayList;
import java.util.List;

public class GearRatio {
    private final List<String> input;

    public GearRatio(List<String> input) {
        this.input = input;
    }

    public int FindSumOfPartNumbers(boolean findGearRatios){
        List<CharacterPosition> characterPositions = new ArrayList<>();
        List<PotentialPart> potentialParts = new ArrayList<>();
        parseInputForPartsAndSpecialCharacters(characterPositions, potentialParts);
        int sumOfParts = getSumOfParts(findGearRatios, characterPositions, potentialParts);
        return sumOfParts;
    }

    private static int getSumOfParts(boolean findGearRatios, List<CharacterPosition> characterPositions, List<PotentialPart> potentialParts) {
        int sumOfParts = 0;
        for (CharacterPosition characterPosition : characterPositions) {
            List<PotentialPart> linkedParts = new ArrayList<>();
            for (PotentialPart potentialPart : potentialParts) {
                if (characterPosition.xPos <= potentialPart.xPosition + 1 &&
                    characterPosition.xPos >= potentialPart.xPosition - 1 &&
                    characterPosition.yPos <= potentialPart.yPosition + potentialPart.numberOfDigits &&
                    characterPosition.yPos >= potentialPart.yPosition - 1) {
                    linkedParts.add(potentialPart);
                }
            }
            if (findGearRatios && characterPosition.isGear && linkedParts.size() == 2) {
                sumOfParts = sumOfParts + linkedParts.get(0).value * linkedParts.get(1).value;
            } else if (!findGearRatios) {
                for (PotentialPart part : linkedParts) {
                    sumOfParts += part.value;
                }
            }
        }
        return sumOfParts;
    }

    private void parseInputForPartsAndSpecialCharacters(List<CharacterPosition> characterPositions, List<PotentialPart> potentialParts) {
        for (int xPos = 0; xPos < input.size(); xPos++) {
            String currentLine = input.get(xPos);
            int startOfPart = -1;
            for (int yPos = 0; yPos < currentLine.length(); yPos++) {

                char currentChar = currentLine.charAt(yPos);

                if (Character.isDigit(currentChar)) {
                    if (startOfPart == -1) {
                        startOfPart = yPos;
                    }
                } else {
                    if (currentChar != '.') {
                        characterPositions.add(new CharacterPosition(xPos,yPos, currentChar == '*'));
                    }
                }
                if (startOfPart != -1) {
                    if (yPos == currentLine.length() - 1 || !Character.isDigit(currentLine.charAt(yPos + 1))) {
                        int numberOfDigits = yPos + 1 - startOfPart;
                        int valueOfPart = Integer.parseInt(currentLine.substring(startOfPart, startOfPart + numberOfDigits));
                        potentialParts.add(new PotentialPart(xPos, startOfPart, numberOfDigits, valueOfPart));
                        startOfPart = -1;
                    }
                }
            }
        }
    }

    private class CharacterPosition {
        private final int xPos;
        private final int yPos;
        private final boolean isGear;

        public CharacterPosition(int xPos, int yPos, boolean isGear){
            this.xPos = xPos;
            this.yPos = yPos;
            this.isGear = isGear;
        }
    }
    private class PotentialPart {
        private final int xPosition;
        private final int yPosition;
        private final int numberOfDigits;
        private final int value;

        public PotentialPart (int xPosition, int yPosition, int numberOfDigits, int value) {
            this.xPosition = xPosition;
            this.yPosition = yPosition;
            this.numberOfDigits = numberOfDigits;
            this.value = value;
        }
    }
}
