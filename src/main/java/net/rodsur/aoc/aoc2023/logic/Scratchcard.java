package net.rodsur.aoc.aoc2023.logic;

import java.util.*;

public class Scratchcard {
    private final List<String> input;

    public Scratchcard (List<String> input) {
        this.input = input;
    }

    public int findPointsValue() {
        int pointsValue = 0;
        for (String line : input) {
            String[] splitLine = line.split("[:\\|]");
            List<Integer> winningNumbers = getIntListFromString(splitLine[1]);
            List<Integer> ourNumbers = getIntListFromString(splitLine[2]);
            int matches = getMatchesForCard(winningNumbers, ourNumbers);
            if (matches <= 2) {
                pointsValue += matches;
            } else {
                pointsValue += Math.pow(2,matches-1);
            }
        }
        return pointsValue;
    }

    private int getMatchesForCard(List<Integer> winningNumbers, List<Integer> ourNumbers) {
        int matches = 0;
        for (Integer winningNumber : winningNumbers) {
            for (Integer ourNumber : ourNumbers) {
                if (winningNumber.equals(ourNumber)) {
                    matches++;
                }
            }
        }
        return matches;
    }

    public int findTotalAmountOfScratchCards() {
        int totalAmountOfScratchCards = 0;
        LinkedList<Integer> cardsToBeProcessed = new LinkedList<>();
        HashMap<Integer, Integer> winningsPerCard = new HashMap<>();
        for (int i = 0; i < input.size(); i++) {
            cardsToBeProcessed.add(i+1);
        }
        while (!cardsToBeProcessed.isEmpty()) {
            int currentCard = cardsToBeProcessed.pop();
            if (!winningsPerCard.containsKey(currentCard)) {
                winningsPerCard.put(currentCard, findWinningsFromCard(currentCard));
            }
            for (int i = currentCard + 1; i <= currentCard + winningsPerCard.get(currentCard); i++) {
                cardsToBeProcessed.add(i);
            }
            totalAmountOfScratchCards++;
        }
        return totalAmountOfScratchCards;
    }

    private int findWinningsFromCard(int card) {
        String cardString = input.get(card - 1);
        String[] explodedcardString = cardString.split("[:\\|]");
        List<Integer> winningNumbers = getIntListFromString(explodedcardString[1]);
        List<Integer> ourNumbers = getIntListFromString(explodedcardString[2]);
        return getMatchesForCard(winningNumbers,ourNumbers);
    }

    private List<Integer> getIntListFromString(String inputString) {
        List<Integer> outputList = new ArrayList<>();
        String[] explodedString = inputString.split(" ");
        for (String string : explodedString) {
            if (!string.isEmpty()) {
                outputList.add(Integer.parseInt(string));
            }
        }
        return outputList;
    }
}
