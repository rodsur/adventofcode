package net.rodsur.aoc.aoc2023.logic;

import java.util.*;

public class TrebuchetCalibration {

    private final List<String> calibrationDocument;
    private final Map<String, Integer> stringValueMap = Map.of(
            "one",1,
            "two", 2,
            "three", 3,
            "four", 4,
            "five", 5,
            "six", 6,
            "seven", 7,
            "eight", 8,
            "nine", 9
    );
    public TrebuchetCalibration(List<String> input) {
        calibrationDocument = input;
    }

    public int findAndAddCalibrationSums(boolean interpretSpelledDigits) {
        int calibrationSum = 0;
        for (String calibrationLine : calibrationDocument) {
            int firstInt;
            int lastInt;
            int lineValue;
            List<Integer> numbers = new ArrayList<>();

            for (int i = 0; i < calibrationLine.length(); i++) {
                if(Character.isDigit(calibrationLine.charAt(i))) {
                    numbers.add(Character.getNumericValue(calibrationLine.charAt(i)));
                }

                if (interpretSpelledDigits) {
                    for (Map.Entry<String, Integer> entry : stringValueMap.entrySet()) {
                        if (calibrationLine.indexOf(entry.getKey(), i) == i) {
                            numbers.add(entry.getValue());
                            break;
                        }
                    }
                }
            }

            firstInt = numbers.get(0);
            lastInt = numbers.get(numbers.size()-1);
            lineValue = firstInt * 10 + lastInt;
            calibrationSum += lineValue;
        }
        return calibrationSum;
    }
}
