package net.rodsur.aoc.aoc2023.puzzles;

import net.rodsur.aoc.aoc2023.logic.TrebuchetCalibration;
import net.rodsur.aoc.common.aocPuzzle;

import java.util.List;

public class Day01 implements aocPuzzle {
    @Override
    public String solvePart1(List<String> input) {
        TrebuchetCalibration trebuchetCalibration = new TrebuchetCalibration(input);
        return String.valueOf(trebuchetCalibration.findAndAddCalibrationSums(false));
    }

    @Override
    public String solvePart2(List<String> input) {
        TrebuchetCalibration trebuchetCalibration = new TrebuchetCalibration(input);
        return String.valueOf(trebuchetCalibration.findAndAddCalibrationSums(true));
    }
}
