package net.rodsur.aoc.aoc2023.puzzles;

import net.rodsur.aoc.aoc2023.logic.CubeCounter;
import net.rodsur.aoc.common.aocPuzzle;

import java.util.List;

public class Day02 implements aocPuzzle {
    @Override
    public String solvePart1(List<String> input) {
        CubeCounter cubeCounter = new CubeCounter(input);
        return String.valueOf(cubeCounter.findSumOfValidGames());
    }

    @Override
    public String solvePart2(List<String> input) {
        CubeCounter cubeCounter = new CubeCounter(input);
        return String.valueOf(cubeCounter.findPowersOfMinimumCubes());
    }
}
