package net.rodsur.aoc.aoc2023.puzzles;

import net.rodsur.aoc.aoc2023.logic.GearRatio;
import net.rodsur.aoc.common.aocPuzzle;

import java.util.List;

public class Day03 implements aocPuzzle {
    @Override
    public String solvePart1(List<String> input) {
        GearRatio gearRatio = new GearRatio(input);
        return String.valueOf(gearRatio.FindSumOfPartNumbers(false));
    }

    @Override
    public String solvePart2(List<String> input) {
        GearRatio gearRatio = new GearRatio(input);
        return String.valueOf(gearRatio.FindSumOfPartNumbers(true));
    }
}
