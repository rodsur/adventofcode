package net.rodsur.aoc.aoc2023.puzzles;

import net.rodsur.aoc.aoc2023.logic.Scratchcard;
import net.rodsur.aoc.common.aocPuzzle;

import java.util.List;

public class Day04 implements aocPuzzle {
    @Override
    public String solvePart1(List<String> input) {
        Scratchcard scratchcard = new Scratchcard(input);
        return String.valueOf(scratchcard.findPointsValue());
    }

    @Override
    public String solvePart2(List<String> input) {
        Scratchcard scratchcard = new Scratchcard(input);
        return String.valueOf(scratchcard.findTotalAmountOfScratchCards());
    }
}
