package net.rodsur.aoc.common;

import java.util.List;

public interface aocPuzzle {

    String solvePart1(List<String> input);

    String solvePart2(List<String> input);
}
