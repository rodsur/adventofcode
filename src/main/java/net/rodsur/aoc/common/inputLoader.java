package net.rodsur.aoc.common;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class inputLoader {

    public static List<String> loadFile(String inputPath) {
        List<String> output = new ArrayList<>();
        try {
            File inputFile = new File(inputPath);
            Scanner scanner = new Scanner(inputFile);
            while(scanner.hasNext()) {
                String line = scanner.nextLine();
                output.add(line);
            }
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
        return output;
    }
}
