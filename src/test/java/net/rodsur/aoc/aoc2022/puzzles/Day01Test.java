package net.rodsur.aoc.aoc2022.puzzles;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import net.rodsur.aoc.common.inputLoader;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class Day01Test {

    static Day01 puzzle;
    private List<String> input;
    private String output;

    @BeforeAll
    public static void setup() {
        puzzle = new Day01();
    }

    @AfterEach
    void clearOutput() {
        output = "0";
    }

    @Test
    void solvePart1Example() {
        input = inputLoader.loadFile("src/test/resources/2022/Day01Part1Example.txt");
        output = puzzle.solvePart1(input);

        assertEquals("24000", output);
    }

    @Test
    void solvePart1() {
        input = inputLoader.loadFile("src/test/resources/2022/Day01.txt");
        output = puzzle.solvePart1(input);

        assertEquals("70720", output);
    }

    @Test
    void solvePart2() {
        input = inputLoader.loadFile("src/test/resources/2022/Day01.txt");
        output = puzzle.solvePart2(input);

        assertEquals("207148", output);
    }
}