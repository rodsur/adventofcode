package net.rodsur.aoc.aoc2022.puzzles;

import net.rodsur.aoc.common.inputLoader;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class Day03Test {

    static Day03 puzzle;
    private List<String> input;
    private String output;

    @BeforeAll
    public static void setup() {
        puzzle = new Day03();
    }

    @AfterEach
    void clearOutput() {
        output = "0";
    }

    @Test
    void solvePart1Example() {
        input = inputLoader.loadFile("src/test/resources/2022/Day03Example.txt");
        output = puzzle.solvePart1(input);

        assertEquals("157", output);
    }

    @Test
    void solvePart1() {
        input = inputLoader.loadFile("src/test/resources/2022/Day03.txt");
        output = puzzle.solvePart1(input);

        assertEquals("7997", output);
    }

    @Test
    void solvePart2Example() {
        input = inputLoader.loadFile("src/test/resources/2022/Day03Example.txt");
        output = puzzle.solvePart2(input);

        assertEquals("70", output);
    }

    @Test
    void solvePart2() {
        input = inputLoader.loadFile("src/test/resources/2022/Day03.txt");
        output = puzzle.solvePart2(input);

        assertEquals("2545", output);
    }
}