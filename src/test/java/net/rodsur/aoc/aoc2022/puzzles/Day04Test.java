package net.rodsur.aoc.aoc2022.puzzles;

import net.rodsur.aoc.common.inputLoader;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class Day04Test {

    static Day04 puzzle;
    private List<String> input;
    private String output;

    @BeforeAll
    public static void setup() {
        puzzle = new Day04();
    }

    @AfterEach
    void clearOutput() {
        output = "0";
    }

    @Test
    void solvePart1Example() {
        input = inputLoader.loadFile("src/test/resources/2022/Day04Example.txt");
        output = puzzle.solvePart1(input);

        assertEquals("2", output);
    }

    @Test
    void solvePart1() {
        input = inputLoader.loadFile("src/test/resources/2022/Day04.txt");
        output = puzzle.solvePart1(input);

        assertEquals("448", output);
    }

    @Test
    void solvePart2Example() {
        input = inputLoader.loadFile("src/test/resources/2022/Day04Example.txt");
        output = puzzle.solvePart2(input);

        assertEquals("4", output);
    }

    @Test
    void solvePart2() {
        input = inputLoader.loadFile("src/test/resources/2022/Day04.txt");
        output = puzzle.solvePart2(input);

        assertEquals("794", output);
    }
}