package net.rodsur.aoc.aoc2022.puzzles;

import net.rodsur.aoc.common.inputLoader;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Day06Test {

    static Day06 puzzle;
    private List<String> input;
    private String output;

    @BeforeAll
    public static void setup() {
        puzzle = new Day06();
    }

    @AfterEach
    void clearOutput() {
        output = "0";
    }

    @Test
    void solvePart1Examples() {
        input = new ArrayList<>(List.of("bvwbjplbgvbhsrlpgdmjqwftvncz"));
        output = puzzle.solvePart1(input);
        assertEquals("5", output);

        input = new ArrayList<>(List.of("nppdvjthqldpwncqszvftbrmjlhg"));
        output = puzzle.solvePart1(input);
        assertEquals("6", output);

        input = new ArrayList<>(List.of("nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg"));
        output = puzzle.solvePart1(input);
        assertEquals("10", output);

        input = new ArrayList<>(List.of("zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw"));
        output = puzzle.solvePart1(input);
        assertEquals("11", output);
    }

    @Test
    void solvePart1() {
        input = inputLoader.loadFile("src/test/resources/2022/Day06.txt");
        output = puzzle.solvePart1(input);

        assertEquals("1343", output);
    }

    @Test
    void solvePart2Example() {
        input = new ArrayList<>(List.of("mjqjpqmgbljsphdztnvjfqwrcgsmlb"));
        output = puzzle.solvePart2(input);
        assertEquals("19", output);

        input = new ArrayList<>(List.of("bvwbjplbgvbhsrlpgdmjqwftvncz"));
        output = puzzle.solvePart2(input);
        assertEquals("23", output);

        input = new ArrayList<>(List.of("nppdvjthqldpwncqszvftbrmjlhg"));
        output = puzzle.solvePart2(input);
        assertEquals("23", output);

        input = new ArrayList<>(List.of("nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg"));
        output = puzzle.solvePart2(input);
        assertEquals("29", output);

        input = new ArrayList<>(List.of("zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw"));
        output = puzzle.solvePart2(input);
        assertEquals("26", output);
    }

    @Test
    void solvePart2() {
        input = inputLoader.loadFile("src/test/resources/2022/Day06.txt");
        output = puzzle.solvePart2(input);

        assertEquals("2193", output);
    }
}