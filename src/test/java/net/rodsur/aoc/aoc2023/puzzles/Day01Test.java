package net.rodsur.aoc.aoc2023.puzzles;

import net.rodsur.aoc.common.inputLoader;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;


class Day01Test {

    static Day01 puzzle;
    private List<String> input;
    private String output;

    @BeforeAll
    public static void setup() {
        puzzle = new Day01();
    }

    @AfterEach
    void clearOutput() {
        output = "0";
    }

    @Test
    void solvePart1Example() {
        input = inputLoader.loadFile("src/test/resources/2023/Day01Part1Example");
        output = puzzle.solvePart1(input);

        assertEquals("142", output);
    }

    @Test
    void solvePart1() {
        input = inputLoader.loadFile("src/test/resources/2023/Day01");
        output = puzzle.solvePart1(input);

        assertEquals("55538", output);
    }
    @Test
    void solvePart2Example() {
        input = inputLoader.loadFile("src/test/resources/2023/Day01Part2Example");
        output = puzzle.solvePart2(input);

        assertEquals("281", output);
    }

    @Test
    void solvePart2() {
        input = inputLoader.loadFile("src/test/resources/2023/Day01");
        output = puzzle.solvePart2(input);

        assertEquals("54875", output); //not 54868
    }
}