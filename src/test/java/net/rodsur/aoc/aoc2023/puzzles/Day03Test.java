package net.rodsur.aoc.aoc2023.puzzles;

import net.rodsur.aoc.common.inputLoader;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;


class Day03Test {

    static Day03 puzzle;
    private List<String> input;
    private String output;

    @BeforeAll
    public static void setup() {
        puzzle = new Day03();
    }

    @AfterEach
    void clearOutput() {
        output = "0";
    }

    @Test
    void solvePart1Example() {
        input = inputLoader.loadFile("src/test/resources/2023/Day03Example");
        output = puzzle.solvePart1(input);

        assertEquals("4361", output);
    }

    @Test
    void solvePart1() {
        input = inputLoader.loadFile("src/test/resources/2023/Day03");
        output = puzzle.solvePart1(input);

        assertEquals("526404", output);
    }

    @Test
    void solvePart2Example() {
        input = inputLoader.loadFile("src/test/resources/2023/Day03Example");
        output = puzzle.solvePart2(input);

        assertEquals("467835", output);
    }

    @Test
    void solvePart2() {
        input = inputLoader.loadFile("src/test/resources/2023/Day03");
        output = puzzle.solvePart2(input);

        assertEquals("84399773", output);
    }
}